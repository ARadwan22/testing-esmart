import testData from '../fixtures/testData.json';

class LoginPage{

  login(username, password,signManager,signPassword,directmanager,directPassword,contractor,contractorPassword,reviewer,reviewerPassword,quality,qualityPassword){

    cy.visit(testData.stagingUrl);
    this.Emailinput.type();
    this.Passwordinput.type();
    this.LoginBtn.click();
  }

  



  get Emailinput() {

   return cy.get(':nth-child(2) > .form-control');

  }
  
  get Passwordinput(){

    return cy.get('.border-top > .form-control');

  }

  get LoginBtn(){

    return cy.get('.btn');
  }
  
  }

export default new LoginPage();