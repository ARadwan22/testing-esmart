import testData from '../fixtures/testData.json';

class LoginPage{

  login(username, password,signManager,signPassword,directmanager,directPassword,contractor,contractorPassword,reviewer,reviewerPassword,quality,qualityPassword){

    cy.visit(testData.stagingUrl);
    this.Emailinput.type(testData.username);
    this.Passwordinput.type(testData.password);
    this.LoginBtn.click();
  }

  


  get Emailinput() {

   return cy.get('#__BVID__12')

  }
  
  get Passwordinput(){

    return cy.get('#__BVID__14')

  }

  get LoginBtn(){

    return cy.get('.btn');
  }
  
  }

export default new LoginPage();